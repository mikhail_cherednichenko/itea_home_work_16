/*Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище), rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.*/

class Worker {
    constructor(name, surname, rate, days, currency = "грн.") {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
        this.currency = currency;
    };

    getSalary() {
        return `Зарплата працівника ${this.rate * this.days} ${this.currency}`;
    }
};

const mikola = new Worker("Mikola", "Shevchenko", 500, 30);

console.log(mikola.getSalary());

/*Реалізуйте клас MyString, який матиме такі методи: метод reverse(), який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(), який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.*/

class MyString {

    reverse(temp) {
        return temp.split("").reverse().join("");
    };

    ucFirst(temp) {
        return temp[0].toUpperCase() + temp.slice(1);
    };

    ucWords(temp) {
        temp = temp.split(" ");
        for (let i = 0; i < temp.length; i++) {
            temp[i] = this.ucFirst(temp[i]);
        };
        return temp.join(" ");
    };
};

const myString = new MyString();

console.log(myString.reverse("mystring"));
console.log(myString.ucFirst("mystring"));
console.log(myString.ucWords("hello world"));

/* Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів. */

class Phone {
    constructor(number, model, weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    };

    receiveCall(name) {
        console.log(`Телефонує ${name}`);
    };

    getNumber() {
        return this.number;
    };
};

const phone1 = new Phone(177100004, "iPhone", 320);
const phone2 = new Phone(806944411, "Samsung", 220);
const phone3 = new Phone(130777852, "Xiaomi", 280);

console.log(phone1);
console.log(phone2);
console.log(phone3);

phone1.receiveCall(`Ivan`);
phone2.receiveCall(`Petro`);
phone3.receiveCall(`Alina`);

console.log(phone1.getNumber());
console.log(phone2.getNumber());
console.log(phone3.getNumber());

/* Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна. */

class Car {
    constructor(маркаАвтомобіля, класАвтомобіля, вага, водій, мотор) {
        this[`марка автомобіля`] = маркаАвтомобіля;
        this[`клас автомобіля`] = класАвтомобіля;
        this.вага = вага;
        this.водій = водій;
        this.мотор = мотор;
    };

    start() {
        console.log("Поїхали");
    };
    
    stop() {
        console.log("Зупиняємося");
    };
    
    turnRight() {
        console.log("Поворот праворуч");
    };
    
    turnLeft() {
        console.log("Поворот ліворуч");
    };

    toString() {
        console.log(this);
    };
};

class Engine {
    constructor(потужність, виробник) {
        this.потужність = потужність;
        this.виробник = виробник;
    };
};

class Driver {
    constructor(ПІБ, стажВодіння) {
        this.ПІБ = ПІБ;
        this["стаж водіння"] = стажВодіння;
    };
};

const водій = new Driver("Українець М.І.", 10);
const мотор = new Engine(250, "ZAZ");
const автомобіль = new Car("ZAZ", "sedan", 1500, водій, мотор);

автомобіль.toString();
автомобіль.start();
автомобіль.stop();
автомобіль.turnRight();
автомобіль.turnLeft();

/* Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю. */

class Lorry extends Car {
    constructor(вантажопідйомність, маркаАвтомобіля, вага, водій, мотор) {
        super(маркаАвтомобіля, "Вантажівка", вага, водій, мотор);
        this[`вантажопід'йомність`] = вантажопідйомність;
    };
};

class SportCar extends Car {
    constructor(швидкість, маркаАвтомобіля, вага, водій, мотор) {
        super(маркаАвтомобіля, "Спорткар", вага, водій, мотор);
        this[`гранична швидкість`] = швидкість;
    };
};

const вантажівка = new Lorry(1200,"ГАЗ",4500,водій,мотор);
const спортКар = new SportCar(300,"Lamborgini",4500,водій,мотор);

вантажівка.toString();
спортКар.toString();

/* Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.*/

const instancesOfAnimal = new Set();

class Animal {
    constructor(food, location) {
        this.food = food;
        this.location = location;
        instancesOfAnimal.add(this);
    };

    makeNoise() {
        console.log(`Ця тварина шумить`)
    }; 
    
    eat() {
        console.log(`Ця тварина їсть`)
    };
    
    sleep() {
        console.log(`Ця тварина спить`)
    };
};

class Dog extends Animal{
    constructor(location) {
        super("м'ясо", location);
        this.name = "Собака";
        this.noise = "гав-гав";
    };

    makeNoise() {
        console.log(`${this.name} каже ${this.noise}`);
    }; 
    
    eat() {
        console.log(`${this.name} їсть ${this.food}`);
    };
};

class Cat extends Animal{
    constructor(location) {
        super("м'ясо", location);
        this.name = "Кіт";
        this.noise = "мяу-мяу";
    };

    makeNoise() {
        console.log(`${this.name} каже ${this.noise}`);
    }; 
    
    eat() {
        console.log(`${this.name} їсть ${this.food}`);
    };
};

class Horse extends Animal{
    constructor(location) {
        super("насіння", location);
        this.name = "Кінь";
        this.noise = "і-го-го";
    };

    makeNoise() {
        console.log(`${this.name} каже ${this.noise}`);
    }; 
    
    eat() {
        console.log(`${this.name} їсть ${this.food}`);
    };
};

const cat = new Cat("дім");
const dog = new Dog("двір");
const horse = new Horse("стайня");

cat.makeNoise();
cat.eat();
dog.makeNoise();
dog.eat();
horse.makeNoise();
horse.eat();

/*Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара. */

class Ветеринар {
    treatAnimal(animal) {
        console.log(`${animal.name} живе у ${animal.location} та їсть ${animal.food}`);
    };

    main(animal) {
        for (let i = 0; i < animal.length; i++) {
            this.treatAnimal(animal[i]);
        };
    };
};

const ветеринар = new Ветеринар();

ветеринар.main(Array.from(instancesOfAnimal));